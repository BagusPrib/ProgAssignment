package animal;

import java.util.Scanner;

public class Animals{
	protected String name;
	protected int length;
	private String kind;
	
	//Constructor animal
	public Animals(String name, int length, String kind){
		this.name = name;
		this.length = length;
		this.kind = kind;
	}
	
	//getter untuk instance variable
	public String getName(){
		return this.name;
	}
	
	public int getLength(){
		return this.length;
	}
	
	public String getKind(){
		return this.kind;
	}
	
	//Membuat binatang yang ada di Javari Park
	public static Animals[] create(String species){
		Scanner input = new Scanner(System.in);
		System.out.print(species + ": ");
		int number = Integer.parseInt(input.nextLine());
		//Jika nomor 0, maka tidak dibuatkan binatangnya
		if(number == 0){
			return null;
		}
		else{
			System.out.println("Provide the information of " + species + "(s):");
			String[] info = input.nextLine().split(",");
			Animals[] pack = new Animals[info.length];
			//looping untuk setiap input binatang
			for(int i = 0; i < info.length; i++){
				String[] detail = info[i].split("\\|");	
				//switch untuk menyesuaikan jenis hewan
				switch(species){
					case "cat" : 
					pack[i] = new Cats(detail[0], Integer.parseInt(detail[1]));
					break;
					case "eagle" : 
					pack[i] = new Eagles(detail[0], Integer.parseInt(detail[1]));
					break;
					case "hamster" : 
					pack[i] = new Hamsters(detail[0], Integer.parseInt(detail[1]));
					break;
					case "parrot" : 
					pack[i] = new Parrots(detail[0], Integer.parseInt(detail[1]));
					break;
					case "lion" : 
					pack[i] = new Lions(detail[0], Integer.parseInt(detail[1]));
					break;
				}
			}
			return pack;
		}
	}
	
	//method mengunjungi hewan
	public static void visit(Animals animal, String name, String species){
		//mencari jenis hewan yang akan dikunjungi
		if(species.equals("cat")){
			((Cats)animal).visit(name);
		}
		else if(species.equals("eagle")){
			((Eagles)animal).visit(name);
		}
		else if(species.equals("hamster")){
			((Hamsters)animal).visit(name);
		}
		else if(species.equals("parrot")){
			((Parrots)animal).visit(name);
		}
		else{
			((Lions)animal).visit(name);
		}
		System.out.println("Back to office!");
	}	
}