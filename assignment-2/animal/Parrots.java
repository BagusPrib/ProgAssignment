package animal;

import java.util.Scanner;

public class Parrots extends Animals{
	
	//Constructor beo
	public Parrots(String name, int length){
		super(name, length, "pet");
	}
	
	//perintah untuk beo
	public void fly(){
		System.out.println(this.name + " makes a voice: FLYYYY...");
	}
	
	public void imitate(String speak){
		System.out.println(this.name + " makes a voice: " + speak.toUpperCase());
	}
	
	//Memasukan perintah sesuai perintah yang tersedia
	public void visit(String name){
		Scanner input = new Scanner(System.in);
		System.out.println("1: Order to fly 2: Do conversation");
		String command = input.nextLine();
		if(command.equals("1")){
			this.fly();
		}
		else if(command.equals("2")){
			System.out.print("You say: ");
			String talk = input.nextLine();
			this.imitate(talk);
		}
		else{
			System.out.println(this.name + " says: HM?");
		}
	}
}