package animal;

import java.util.Scanner;

public class Hamsters extends Animals{
	
	//Constructor hamster
	public Hamsters(String name, int length){
		super(name, length, "pet");
	}
	
	//perintah untuk hamster
	public void gnaw(){
		System.out.println(this.name + " makes a voice: Ngkkrit.. Ngkkrrriiit");
	}
	
	public void wheelRun(){
		System.out.println(this.name + " makes a voice: Trrr... Trrr...");
	}
	
	//Memasukan perintah sesuai perintah yang tersedia
	public void visit(String name){
		Scanner input = new Scanner(System.in);
		System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
		String command = input.nextLine();
		if(command.equals("1")){
			this.gnaw();
		}
		else if(command.equals("2")){
			this.wheelRun();
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}