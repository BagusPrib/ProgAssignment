package cage;
import animal.*;

import java.util.Scanner;

public class CageTools{
	private static Cage[][] cats;
	private static Cage[][] eagles;
	private static Cage[][] hamsters;
	private static Cage[][] parrots;
	private static Cage[][] lions;
	
	//Method untuk membuat layout kandangnya
	public static void createCage(Animals[] pack, String species, String location){
		//Jika binatang tidak ada, tidak dibuatkan kandang
		if(pack == null){
			return;
		}
		Cage[][] cage = checkCage(species, pack.length);
		int count = 0;
		//Melakukan looping sesuai jumlah lantai (3)
		for(int i = 0; i < 3; i++){
			//Jika semua hewan sudah dibuat, berhenti memuat layout
			if(pack.length <= count){
				return;
			}
			//Jika jumlah hewan dibawah 3
			if(pack.length/3 == 0){
				cageInput(cage, i, 0, pack[count], species, location);
				count += 1;
			}
			else{
				//Looping untuk jumlah hewan tiap lantai
				for(int j = 0; j < pack.length/3; j++){
					//Jika jumlah hewan tidak habis dibagi 3
					if(i == 2 && pack.length % 3 > 0){
						//Jika sisa pembagian 1
						if(pack.length % 3 == 1){
							cageInput(cage, i, j + 1, pack[count + 1], species, location);
						}
						//Jika sisa pembagian 2
						else{
							//Looping 2 kali untuk memasukan hewan
							for(int k = 1; k <= pack.length % 3; k++){
								cageInput(cage, i - k, j + k, pack[count + k], species, location);
							}
						}
					}
					cageInput(cage, i, j, pack[count], species, location);
					count += 1;
				}
			}
		}
	}
	
	//Memasukan hewan ke dalam kandang
	public static void cageInput(Cage[][] cage, int level, int position, Animals animal, String species, String location){
		//Menyesuaikan lokasi sesuai jenis hewannya
		if(species.equals("cat") || species.equals("parrot") || species.equals("hamster")){
			cage[level][position] = new Cage(animal, location);
		}
		else{
			cage[level][position] = new Cage(animal, location);
		}
	}
	
	//Mencetak hewan sesuai urutan pembuatan
	public static void printCage(String species, String location){
		Cage[][] cage = checkCage(species);
		//Jika tidak ada kandang
		if(cage == null){
			return;
		}
		System.out.print("\nlocation: " + location);
		//Looping dua kali untuk mencetak sesuai lantai
		for(int i = 2; i >= 0; i--){
			System.out.print("\nlevel " + (i+1) + ": ");
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null){
					continue;
				}
				System.out.print(cage[i][j].getName() + " (" + cage[i][j].getLength() + " - " + cage[i][j].getType() + "), ");
			}
		}
		rearrangeCage(cage);
	}
	
	//Method untuk menata ulang kandang
	public static void rearrangeCage(Cage[][] cage){
		System.out.print("\n\nAfter rearrangement...");
		//Looping untuk menukar lantai
		for(int i = 2; i > 0; i--){
			Cage[] temp = cage[i];
			cage[i] = cage[i-1];
			cage[i-1] = temp;
		}
		int count = 1;
		//Looping untuk menukar posisi per lantai
		for(int i = 2; i >= 0; i--){
			while(count != cage[i].length-1){
				for(int j = 0; j < cage[i].length-count; j++){
					Cage temp = cage[i][j];
					cage[i][j] = cage[i][j+1];
					cage[i][j+1] = temp;
					count += 1;
				}
				if(count == cage[i].length){
					break;
				}
			}
			count = 0;
		}
		for(int i = 2; i >= 0; i--){
			System.out.print("\nlevel " + (i+1) + ": ");
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null){
					continue;
				}
				System.out.print(cage[i][j].getAnimal().getName() + " (" + cage[i][j].getAnimal().getLength() + " - " + cage[i][j].getType() + "), ");
			}
		}
		System.out.print("\n");
	}
	
	//method untuk menghitung jumlah hewan
	public static int countCage(String species){
		Cage[][] cage = checkCage(species);
		//Jika kandang kosong, maka jumlah 0
		if(cage == null){
			return 0;
		}
		//Looping untuk menghitung jumlah hewan
		int count = 0;
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] != null){
					count += 1;
				}			
			}
		}
		return count;
	}
	
	public static void visitCage(String species){
		Cage[][] cage = checkCage(species);
		if(cage == null){
			System.out.println("There are no " + species + " in Javari Park! Back to the office!");
			return;
		}
		Scanner input = new Scanner(System.in);
		System.out.print("Mention the name of " + species + " you want to visit: ");
		String name = input.nextLine();
		boolean found = false;
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null || cage[i] == null){
					continue;
				}
				else if(cage[i][j].getAnimal().getName().equalsIgnoreCase(name)){
					found = true;
					System.out.println("You are visiting " + cage[i][j].getAnimal().getName()
					+ " (" + species + ") now," + "what would you like to do?");
					cage[i][j].getAnimal().visit(cage[i][j].getAnimal(), cage[i][j].getAnimal().getName(), species);
				}
			}
		}
		if(found == false){
			System.out.println("There is no " + species + " with that name! Back to the office!");
		}
	}
	
	public static Cage[][] checkCage(String species, int length){
		if(species.equals("cat")){
			cats = new Cage[3][length];
			return cats;
		}
		else if(species.equals("hamster")){
			hamsters = new Cage[3][length];
			return hamsters;
		}
		else if(species.equals("parrot")){
			parrots = new Cage[3][length];
			return parrots;
		}
		else if(species.equals("eagle")){
			eagles = new Cage[3][length];
			return eagles;
		}
		else{
			lions = new Cage[3][length];
			return lions;
		}
	}
	
	public static Cage[][] checkCage(String species){
		if(species.equals("cat")){
			return cats;
		}
		else if(species.equals("hamster")){
			return hamsters;
		}
		else if(species.equals("parrot")){
			return parrots;
		}
		else if(species.equals("eagle")){
			return eagles;
		}
		else{
			return lions;
		}
	}
}