package cage;
import animal.*;

public class Cage{
	protected Animals animal;
	protected char type;
	
	//getter untuk instance variable Cage dan hewannya	
	public Animals getAnimal(){
		return this.animal;
	}
	
	public char getType(){
		return this.type;
	}
	
	public String getName(){
		return this.animal.getName();
	}
	
	public int getLength(){
		return this.animal.getLength();
	}
	
	//Constructor kandang
	public Cage(Animals animal, String cage){
		this.animal = animal;
		if(cage.equals("indoor")){
			this.type = indoorType(animal.getLength());
		}
		else{
			this.type = outdoorType(animal.getLength());
		}
	}
	
	//Menentukan tipe kandang sesuai lokasinya ditempatkan
	public static char indoorType(int length){
		if(length < 45){
			return 'A';
		}
		else if(length <= 60){
			return 'B';
		}
		else{
			return 'C';
		}
	}
	
	public static char outdoorType(int length){
		if(length < 75){
			return 'A';
		}
		else if(length <= 90){
			return 'B';
		}
		else{
			return 'C';
		}
	}
}