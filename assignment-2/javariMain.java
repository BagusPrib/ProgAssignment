import animal.*;
import cage.*;

import java.util.Scanner;
import java.util.ArrayList;

class JavariMain{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args){
		System.out.println("Welcome to Javari Park!" + 
			"\nInput the Number of Animals");
		//memanggil method method untuk membuat kandang hewan
		CageTools.createCage(Animals.create("cat"), "cat", "indoor");
		CageTools.createCage(Animals.create("lion"), "lion", "outdoor");
		CageTools.createCage(Animals.create("eagle"), "eagle", "outdoor");
		CageTools.createCage(Animals.create("parrot"), "parrot", "indoor");
		CageTools.createCage(Animals.create("hamster"), "hamster", "indoor");
		
		System.out.print("\nAnimals has been successfully recorded!"
		+ "\n============================================="
		+ "\nCage arrangement:");
		
		//mencetak layout Javari Park, lalu di atur ulang penempatannya
		CageTools.printCage("cat", "indoor");
		CageTools.printCage("lion", "outdoor");
		CageTools.printCage("eagle", "outdoor");
		CageTools.printCage("parrot", "indoor");
		CageTools.printCage("hamster", "indoor");
		
		//mencetak jumlah hewan yang tersedia
		System.out.println("\nANIMALS NUMBER:");
		System.out.println("cat:" + CageTools.countCage("cat"));
		System.out.println("lion:" + CageTools.countCage("lion"));
		System.out.println("eagle:" + CageTools.countCage("eagle"));
		System.out.println("parrot:" + CageTools.countCage("parrot"));
		System.out.println("hamster:" + CageTools.countCage("hamster"));
		
		
		System.out.print("============================================= ");
		//meminta perintah apa yang akan dilakukan
		while(true){
			System.out.println("\nWhich animal you want to visit?\n" + 
			"(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
			String command = input.nextLine();
			//meminta input untuk mengunjungi kandang tertentu atau keluar dari Javari Park
			switch(command){
				case "1":
				CageTools.visitCage("cat");
				break;
				case "2":
				CageTools.visitCage("eagle");
				break;
				case "3":
				CageTools.visitCage("hamster");
				break;
				case "4":
				CageTools.visitCage("parrot");
				break;
				case "5":
				CageTools.visitCage("lion");
				break;
				case "99":
				System.out.println("Leaving Javari Park");
				System.exit(0);
				break;
				default :
				System.out.println("Wrong command");
				break;
			}
		}
	}
}