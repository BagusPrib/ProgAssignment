import javari.reader.*;
import javari.park.*;
import javari.writer.*;
import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

public class A3Festival{
    
    private static Scanner input = new Scanner(System.in);
    private static Registration vis;
    
    public static void main(String[] args){
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n"
        + "... Opening default section database from data. ");
        RecordReader record = null;
        CategoryReader category = null;
        AttractionReader attraction = null;
        
        try{
            record = new RecordReader(Paths.get("C:","Users","Acer","Documents","Coding","Git TP","assignment-3","data"));
            category = new CategoryReader(Paths.get("C:","Users","Acer","Documents","Coding","Git TP","assignment-3","data"));
            attraction = new AttractionReader(Paths.get("C:","Users","Acer","Documents","Coding","Git TP","assignment-3","data"));
            System.out.println("\n\n... Loading... Success... System is populating data...\n");
        }
        catch(IOException e){
            System.out.println("... File not found or incorrect file!\n");
            System.out.print("Please provide the source data path: ");
            try{
                String pathNew = input.nextLine();
                record = new RecordReader(Paths.get(pathNew,"animals_Records.csv"));
                category = new CategoryReader(Paths.get(pathNew,"animals_Categories.csv"));
                attraction = new AttractionReader(Paths.get(pathNew,"animals_attractions.csv"));
            }
            catch(IOException a){
                System.out.println("... File not found or incorrect file!\n");
                System.exit(0);
            }
        }
        
        long aCategory = category.countValidRecords();
        long aAttraction = attraction.countValidRecords();
        long aRecord = record.countValidRecords();

        
        System.out.println("Found _" + aCategory + "_ valid sections and _" + category.countInvalidRecords() + "_ invalid sections\n"
        + "Found _" + aAttraction + "_ valid attractions and _" + attraction.countInvalidRecords() + "_ invalid attractions\n"
        + "Found _" + aCategory + "_ valid animal categories and _" + category.countInvalidRecords() + "_ invalid animal categories\n"
        + "Found _" + aRecord + "_ valid animal records and _" + record.countInvalidRecords() + "_ invalid animal records\n");
        
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n\n"
        + "Please answer the questions by typing the number. Type # if you want to return to the previous menu");
        menu1(false);
        String res = "";
        String[] name = vis.getVisitorName().split(" ");
        for(int i = 0; i < name.length; i++){
            res += name[i].substring(0, 1).toUpperCase() + name[i].substring(1);
            if(i != name.length - 1){
                res += "_";
            }
        }
        System.out.print("\n... End of program, write to registration_" + res + ".json ...");
        try{
            RegistrationWriter.writeJSON(vis, Paths.get("C:","Users","Acer","Documents","Coding","Git TP","assignment-3","src","main","java","output"));
        }
        catch(IOException e){
            System.out.println("... File not found or incorrect file!\n");
        }
    }
    
    private static void menu1(boolean condition){
        System.out.print("\nJavari Park has 3 sections:\n"
        + "1. Explore the Mammals\n"
        + "2. World of Aves\n"
        + "3. Reptilian Kingdom\n"
        + "Please choose your preferred section (type the number): ");
        
        String command = input.nextLine();
        if(!(command.equals("1") || command.equals("2") || command.equals("3"))){
            System.out.println("\nPlease input a correct command");
            menu1(condition);
        }
        condition = menu2(command);
        if(condition == false){
            menu1(condition);
        }
        else{
            System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
            String request = input.nextLine();
            if(request.equalsIgnoreCase("y")){
                menu1(false);
            }
        }        
    }
    
    private static boolean menu2(String command){
        if(command.equals("1")){
            System.out.println("\n--Explore the Mammals--");
        }
        else if(command.equals("2")){
            System.out.println("\n--World of Aves--");
        }
        else{
            System.out.println("\n--Reptilian Kingdom--");
        }
        ArrayList<String> sector = Section.getSection(command);
        for(int i = 0; i < sector.size(); i++){
            System.out.println(i + 1 + ". " + sector.get(i));
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        String command2 = input.nextLine();
        if(command2.equals("#")){
            return false;
        }
        try{
            Integer.parseInt(command2);
        }
        catch(NumberFormatException e){
            System.out.println("\nPlease input a correct command");
            return menu2(command);
        }
        String performer = null;
        try{
            performer = sector.get(Integer.parseInt(command2) - 1);
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("\nPlease input a correct command");
            return menu2(command);
        }
        return menu3(performer, command);
    }
    
    private static boolean menu3(String performer, String command){
        System.out.println("\n--" + performer + "--\n"
        + "Attractions by " + performer + ":");
        ArrayList<Attraction> listAttract = Attraction.getAttractions(performer);
        for(int i = 0; i < listAttract.size(); i++){
            System.out.println(i + 1 + ". " + listAttract.get(i).getName());
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        String command3 = input.nextLine();
        if(command3.equals("#")){
            return menu2(command);
        }
        try{
            Integer.parseInt(command3);
        }
        catch(NumberFormatException e){
            System.out.println("\nPlease input a correct command");
            return menu3(performer, command);
        }
        Attraction perform = null;
        try{
            perform = listAttract.get(Integer.parseInt(command3) - 1);
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("\nPlease input a correct command");
            return menu3(performer, command);
        }
        lastCheck(perform, performer, command);
        return true;
    }
    
    public static boolean lastCheck(Attraction perform, String performer, String command){
        System.out.println("\nWow, one more step,\n"
        + "please let us know your name: ");
        String name = input.nextLine();
        vis = Visitor.find(name);
        if(vis == null){
            vis = new Visitor(name);
        }
        System.out.print("\nYeay, final check!\n"
        + "Here is your data, and the attraction you chose:\n"
        + "Name: " + name + "\n"
        + "Attractions: " + perform.getName() + " -> " + perform.getType() + "\n"
        + "With: ");
        for(int i = 0; i < perform.getPerformers().size(); i++){
            System.out.print(perform.getPerformers().get(i).getName());
            if(i != perform.getPerformers().size()-1){
                System.out.print(", ");
            }
            else{
                System.out.println("\n");
            }
        }
        
        System.out.print("Is the data correct? (Y/N): ");
        String confirm = input.nextLine();
        if(confirm.equalsIgnoreCase("n")){
            return lastCheck(perform, performer, command);
        }
        else if(confirm.equals("#")){
            return menu3(performer, command);
        }
        else if(!(confirm.equalsIgnoreCase("y"))){
            System.out.println("\nPlease input a correct command");
            return lastCheck(perform, performer, command);
        }
        else{
            vis.addSelectedAttraction(perform);
            return true;
        }
    }
}