package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration{

    private String name;
    private int id;
    private int count = 1;
    private static ArrayList<Registration> visitors = new ArrayList<Registration>();
    private ArrayList<SelectedAttraction> selectedAttract = new ArrayList<SelectedAttraction>();


    public Visitor(String name){
        this.name = name;
        this.id = count;
        visitors.add(this);
        count += 1;
    }

    public int getRegistrationId(){
        return this.id;
    }

    public String getVisitorName(){
        return this.name;
    }

    public void setVisitorName(String name){
        this.name = name;
    }

    public List<SelectedAttraction> getSelectedAttractions(){
        return selectedAttract;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected){
        selectedAttract.add(selected);
        return true;
    }

    public static Registration find(String name){
        for(int x = 0; x < visitors.size(); x++){
            if(visitors.get(x).getVisitorName().equalsIgnoreCase(name)){
                return visitors.get(x);
            }
        }
        return null;
    }
}