package javari.park;

import javari.animal.*;
import java.util.List;
import java.util.ArrayList;

public class Attraction implements SelectedAttraction{
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();
    
    private static List<Attraction> attractions = new ArrayList<Attraction>();
    
    public Attraction(String[] info){
        this.name = info[1];
        this.type = info[0];
        attractions.add(this);
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getType(){
        return this.type;
    }
    
    public List<Animal> getPerformers(){
        return performers;
    }
    
    public boolean addPerformer(Animal performer){
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
    
    public static List<Attraction> getAttractions(){
        return attractions;
    }
    
    public static ArrayList<Attraction> getAttractions(String performer){
        ArrayList<Attraction> attractionsNew = new ArrayList<Attraction>();
        for(int i = 0; i < attractions.size(); i++){
            if(attractions.get(i).getType().equalsIgnoreCase(performer)){
                attractionsNew.add(attractions.get(i));
            }
        }
        return attractionsNew;
    }
}