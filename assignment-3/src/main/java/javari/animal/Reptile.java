package javari.animal;

public class Reptile extends Animal{
    
    private String special;
    
    public Reptile(String[] detail){
        super(Integer.parseInt(detail[0]), detail[1], detail[2],Gender.parseGender(detail[3])
        ,Double.parseDouble(detail[4]), Double.parseDouble(detail[5]), Condition.parseCondition(detail[7]));
        this.special = detail[6];
    }
    
    public boolean specificCondition(){
        if(this.special.equalsIgnoreCase("tame")){
            return true;
        }
        return true;
    }
}