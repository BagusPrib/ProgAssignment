package javari.animal;

public class Aves extends Animal{
    
    private String special;
    
    public Aves(String[] detail){
        super(Integer.parseInt(detail[0]), detail[1], detail[2],Gender.parseGender(detail[3])
        ,Double.parseDouble(detail[4]), Double.parseDouble(detail[5]), Condition.parseCondition(detail[7]));
        this.special = detail[6];
    }
    
    public boolean specificCondition(){
        if(this.special.equalsIgnoreCase("laying eggs")){
            return false;
        }
        return true;
    }
}