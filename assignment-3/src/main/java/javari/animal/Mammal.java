package javari.animal;

public class Mammal extends Animal{
    
    private String special;
    
    public Mammal(String[] detail){
        super(Integer.parseInt(detail[0]), detail[1], detail[2],Gender.parseGender(detail[3])
        ,Double.parseDouble(detail[4]), Double.parseDouble(detail[5]), Condition.parseCondition(detail[7]));
        this.special = detail[6];
    }
    
    public boolean specificCondition(){
        if(this.getType().equalsIgnoreCase("lion") && this.getGender().equals(Gender.FEMALE)){
            return false;
        }
        if(this.special.equalsIgnoreCase("pregnant")){
            return false;
        }
        return true;
    }
}