package javari.reader;

import javari.park.*;
import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AttractionReader extends CsvReader{
    private long invalid = 0;
    private long valid = 0;
    private boolean circle = false;
    private boolean dancing = false;
    private boolean counting = false;
    private boolean coder = false;
    
    public AttractionReader(Path path) throws IOException{
        super(path);
    }
    
    public long countValidRecords(){
        List<String> data = super.getLines();
        for(int i = 0; i < data.size(); i++){
            String[] info = data.get(i).split(",");
            if(info[1].equalsIgnoreCase("Circles of Fires")
            && (info[0].equalsIgnoreCase("Lion") || info[0].equalsIgnoreCase("Whale") || info[0].equalsIgnoreCase("Eagle"))){
                new Attraction(info);
                if(circle == false){
                    circle = true;
                    valid += 1;
                }
            }
            else if(info[1].equalsIgnoreCase("Dancing Animals")
            && (info[0].equalsIgnoreCase("Parrot") || info[0].equalsIgnoreCase("Cat") || info[0].equalsIgnoreCase("Snake") || info[0].equalsIgnoreCase("Hamster"))){
                new Attraction(info);
                if(dancing == false){
                    dancing = true;
                    valid += 1;
                }
            }
            else if(info[1].equalsIgnoreCase("Counting Masters")
            && (info[0].equalsIgnoreCase("Parrot") || info[0].equalsIgnoreCase("Whale") || info[0].equalsIgnoreCase("Hamster"))){
                new Attraction(info);
                if(counting == false){
                    counting = true;
                    valid += 1;
                }
            }
            else if(info[1].equalsIgnoreCase("Passionate Coders")
            && (info[0].equalsIgnoreCase("Cat") || info[0].equalsIgnoreCase("Hamster") || info[0].equalsIgnoreCase("Snake"))){
                new Attraction(info);
                if(coder == false){
                    coder = true;
                    valid += 1;
                }
            }
            else{
                invalid += 1;
            }
        }
        return valid;
    }
    
    public long countInvalidRecords(){
        return invalid;
    }
}