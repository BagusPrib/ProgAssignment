package javari.reader;

import javari.animal.*;
import javari.park.*;
import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RecordReader extends CsvReader{
    private long invalid = 0;
    private long valid = 0;
    
    public RecordReader(Path path) throws IOException{
        super(path);
    }
    
    public long countValidRecords(){
        List<String> data = super.getLines();
        for(int i = 0; i < data.size(); i++){
            String[] info = data.get(i).split(",");
            Animal animal = null;
            if(info[1].equalsIgnoreCase("Lion") || info[1].equalsIgnoreCase("Cat") 
            || info[1].equalsIgnoreCase("Whale") || info[1].equalsIgnoreCase("Hamster")){
                animal = new Mammal(info);
                valid += 1;
            }
            else if(info[1].equalsIgnoreCase("Snake")){
                animal = new Reptile(info);
                valid += 1;
            }
            else if(info[1].equalsIgnoreCase("Eagle") || info[1].equalsIgnoreCase("Parrot")){
                animal = new Aves(info);
                valid += 1;
            }
            else{
                invalid += 1;
            }
            for(int j = 0; j < Attraction.getAttractions().size(); j++){
                if(Attraction.getAttractions().get(j).getType().equalsIgnoreCase(info[1])){
                    Attraction.getAttractions().get(j).addPerformer(animal);
                }
            }
        }
        return valid;
    }
    
    public long countInvalidRecords(){
        return invalid;
    }
}