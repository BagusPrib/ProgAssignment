package javari.reader;

import javari.park.*;
import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CategoryReader extends CsvReader{
    private long invalid = 0;
    private long valid = 0;
    private boolean mammal = false;
    private boolean reptile = false;
    private boolean aves = false;

    public CategoryReader(Path path) throws IOException{
        super(path);
    }
    
    public long countValidRecords(){
        List<String> data = super.getLines();
        for(int i = 0; i < data.size(); i++){
            String[] info = data.get(i).split(",");
            if(info[2].equalsIgnoreCase("Explore the Mammals")
            && info[1].equalsIgnoreCase("Mammals")
            && (info[0].equalsIgnoreCase("Lion") || info[0].equalsIgnoreCase("Whale") || info[0].equalsIgnoreCase("Cat") || info[0].equalsIgnoreCase("Hamster"))){
                new Section(info);
                if(mammal == false){
                    mammal = true;
                    valid += 1;
                }
            }
            else if(info[2].equalsIgnoreCase("Reptillian Kingdom")
            && info[1].equalsIgnoreCase("Reptiles")
            && info[0].equalsIgnoreCase("Snake")){
                new Section(info);
                if(reptile == false){
                    reptile = true;
                    valid += 1;
                }
            }
            else if(info[2].equalsIgnoreCase("World of Aves")
            && info[1].equalsIgnoreCase("Aves")
            && (info[0].equalsIgnoreCase("Parrot") || info[0].equalsIgnoreCase("Eagle"))){
                new Section(info);
                if(aves == false){
                    aves = true;
                    valid += 1;
                }
            }
            else{
                invalid += 1;
            }
        }
        return valid;
    }

    public long countInvalidRecords(){
        return invalid;
    }
}