public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

	//Constructor WildCat
    public WildCat(String name, double weight, double length) {
        this.name = name;
		this.weight = weight;
		this.length = length;
    }

	//Menghitung BMI dengan mengambil 2 angka dibelakang koma
    public double computeMassIndex() {
        double bmi = weight / Math.pow(length / 100, 2);
		return bmi;
	}
}
