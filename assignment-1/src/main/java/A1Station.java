import java.util.*;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    private static ArrayList<TrainCar> train = new ArrayList<TrainCar>();

    public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input number of cats that you want to process	:");
		int number = Integer.parseInt(input.nextLine());
		System.out.println("Input cat's data :");
		//Melakukan looping sesuai jumlah WildCat yang akan dimasukan
		for(int n = 0;n < number;n++){
			String[] data = input.nextLine().split(",");
			
			String catName = data[0];
			double catWeight = Double.parseDouble(data[1]);
			double catLength = Double.parseDouble(data[2]);
			WildCat catto = new WildCat(catName, catWeight, catLength);
			//Jika kereta belum dibuat, maka akan dibuat gerbong pertama, jika sudah maka akan dimasukan ke gerbong pertama dan gerbong yang sebelumnya sudah ada digeser menjadi gerbong berikutnya
			if(train.size() == 0){
				train.add(new TrainCar(catto));
			}
			else{
				train.add(new TrainCar(catto,train.get(train.size()-1)));
			}
			//Jika beban kereta sudah melewati threshold, maka kereta diberangkatkan, jika tidak melewati threshold dan sudah diakhir inisiasi WildCat, maka kereta diberangkatkan juga
			if(train.get(train.size()-1).computeTotalWeight() > THRESHOLD){
				System.out.println("\nTrain has exceeded threshold, this train will depart to Javari Park soon");
				System.out.println("The remaining cats will be put inside the next train\n");
				System.out.println("The train departs to Javari Park!");
				System.out.print("[LOCO]<--");
				train.get(train.size()-1).printCar();
				System.out.println("Average mass index of all cats: " + String.format("%.2f", train.get(train.size()-1).computeTotalMassIndex()/train.size()));
				//Menentukan tipe berat WildCat
				if(train.get(train.size()-1).computeTotalMassIndex() < 18.5){
					System.out.println("In average, the cats in the train are *underweight*\n");
				}
				else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 25 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 18){
					System.out.println("In average, the cats in the train are *normal*\n");
				}
				else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 30 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 25){
					System.out.println("In average, the cats in the train are *overweight*\n");
				}
				else{
					System.out.println("In average, the cats in the train are *obese*\n");
				}
				train.clear();
			}
			else if(n == number-1){
				System.out.println("The train departs to Javari Park!");
				System.out.print("[Loco]<--");
				train.get(train.size()-1).printCar();
				System.out.println("Average mass index of all cats: " + String.format("%.2f", train.get(train.size()-1).computeTotalMassIndex()/train.size()));
				//Menentukan tipe berat WildCat
				if(train.get(train.size()-1).computeTotalMassIndex() < 18.5){
					System.out.println("In average, the cats in the train are *underweight*");
				}
				else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 25 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 18){
					System.out.println("In average, the cats in the train are *normal*");
				}
				else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() < 30 && train.get(train.size()-1).computeTotalMassIndex()/train.size() >= 25){
					System.out.println("In average, the cats in the train are *overweight*");
				}
				else if(train.get(train.size()-1).computeTotalMassIndex()/train.size() > 30){
					System.out.println("In average, the cats in the train are *obese*");
				}
			}
		}
    }
}
