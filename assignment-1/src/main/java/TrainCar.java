public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    public WildCat cat;
	
	public TrainCar next;

	//Constructor trainCar bila gerbong pertama
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

	//Constructor trainCar bila bukan gerbong pertama
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
		this.next = next;
    }

	//Menghitung beban total kereta beserta isinya
    public double computeTotalWeight() {
		//Jika sudah diakhir gerbong, maka perhitungan terakhir disini, jika bukan maka ditambah lagi dengan gerbong selanjutnya
        if(next == null){
			return EMPTY_WEIGHT + cat.weight;
		}
		else{
			return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
		}
    }

	//Menghitung beban BMI semua WildCat yang ada di dalam kereta
    public double computeTotalMassIndex() {
		//Jika sudah diakhir gerbong, maka perhitungan terakhir disini, jika bukan maka ditambah lagi dengan gerbong selanjutnya
        if(next == null){
			return cat.computeMassIndex();
		}
        else{
			return cat.computeMassIndex() + next.computeTotalMassIndex();
		}
    }

	//Mencetak semua nama WildCat yang berada dalam kereta
    public void printCar() {
		//Jika sudah diakhir gerbong, maka pencetakan terakhir disini, jika bukan maka ditambah lagi dengan gerbong selanjutnya
        if(next == null){
			System.out.println("(" + cat.name + ")");
		}
		else{
			System.out.print("(" + cat.name + ")--");
			next.printCar();
		}
    }
}
