import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class MemoryGame{
    
    //This is the main of this class
    public static void main(String[] args){
        JFrame frame = new JFrame("Javari Memory Game");
        frame.setSize(600,575);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel startMenu = new JPanel(new BorderLayout());
        
        //input bgm into the game
        try{
            File music = new File("sound\\Homelander.wav");
            Clip bgm = AudioSystem.getClip();
            bgm.open(AudioSystem.getAudioInputStream(music));
            bgm.loop(Clip.LOOP_CONTINUOUSLY);
            bgm.start();
        }
        catch(IOException e){
            System.out.println("Music not found");
        }
        catch(LineUnavailableException f){
            System.out.println("Music not found");
        }
        catch(UnsupportedAudioFileException g){
            System.out.println("Unsupported music type");
        }
        JLabel back = new JLabel();
        
        //input background to the start screen
        try {
            BufferedImage pict = ImageIO.read(new File("image\\background.jpg"));
            ImageIcon pic = new ImageIcon(pict);
            back.setIcon(pic);
            back.setSize(600,500);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
        startMenu.add(back, BorderLayout.CENTER);
        
        JButton startButton = new JButton();
        startButton.setText("Start Game");
        startMenu.add(startButton, BorderLayout.SOUTH);
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startMenu.setVisible(false);
                CardManager manage = new CardManager(frame);
            }
        });
        frame.add(startMenu);
        
        frame.setVisible(true);
    }
}
