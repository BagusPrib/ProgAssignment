import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CardManager{
    private ArrayList<Integer> added = new ArrayList<Integer>();
    private ArrayList<Card> cards = new ArrayList<Card>();
    
    //Constructor
    public CardManager(JFrame frame){
        JPanel cardPane = new JPanel(new GridLayout(6,6));
        cardPane.setSize(600,600);
        JPanel menu = new JPanel(new BorderLayout());
        Random random = new Random();
        
        //creating id for all button's picture
        for(int i = 1; i <= 18; i++){
            for(int j = 0; j < 2; j++){
                added.add(i);
            }
        }
        
        Collections.shuffle(added);
        
        //creating button in specified grid
        int count = 0;
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                Card card = new Card(String.valueOf(added.get(count)));
                cardPane.add(card.getCard());
                cards.add(card);
                count += 1;
            }
        }
        menu.add(cardPane, BorderLayout.NORTH);
        
        JButton replay = new JButton();
        replay.setText("Replay");
        menu.add(replay, BorderLayout.WEST);
        replay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Collections.shuffle(added);
                for(int i = 0; i < cards.size(); i++){
                    cards.get(i).reset(String.valueOf(added.get(i)));
                }
                Card.resetAll();
            }
        });
        
        JLabel tries = Card.triesNumber();
        tries.setHorizontalAlignment(JLabel.CENTER);
        tries.setVerticalAlignment(JLabel.CENTER);
        menu.add(tries, BorderLayout.CENTER);
        
        JButton close = new JButton();
        close.setText("Quit");
        menu.add(close, BorderLayout.EAST);
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        
        frame.add(menu);
    }
}