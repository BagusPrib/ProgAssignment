import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EmptyStackException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Card{
    private JButton card;
    private ImageIcon pic;
    private ImageIcon picDef;
    private String picNum;
    private boolean clicked = false;
    
    private static int win = 0;
    private static JLabel tries;
    private static int triesNum = 0;
    private static ArrayList<Card> opened = new ArrayList<Card>();
    private Timer timer = new Timer(500, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //if user click a button when timer is on, shows pop up message
                try{
                    check();
                }
                catch(EmptyStackException z){
                    JOptionPane.showMessageDialog(null, "You should not click anything"
                    + " when game is processing");
                }
            }
        });
    
    //getter for jbutton
    public JButton getCard(){
        return this.card;
    }
    
    //setter for jbutton's condition
    public void setClicked(boolean clicked){
        this.clicked = clicked;
    }
    
    ////getter for jbutton's condition
    public boolean getClicked(){
        return clicked;
    }
    
    //getter for jbutton's picture id
    public String getNum(){
        return picNum;
    }
    
    //getter for jlabel showing moves
    public static JLabel getTries(){
        return tries;
    }
    
    //card constructor
    public Card(String picNum){
        this.card = new JButton();
        this.picNum = picNum;
        
        //if picture doesn't exist, print the error
        try {
            BufferedImage pict = ImageIO.read(new File("image\\cover.png"));
            this.picDef = new ImageIcon(pict);
            pict = ImageIO.read(new File("image\\" + picNum + ".jpg"));
            this.pic = new ImageIcon(pict);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
        this.card.setIcon(picDef);
        this.card.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                click();
                //if 2 card are opened, check the cards
                if(opened.size() == 2){
                    triesNum += 1;
                    tries.setText("Number of tries : " + triesNum);
                    timer.start();
                }
            }
        });
        timer.setRepeats(false);
    }
    
    //for creating or resetting jbutton
    public static JLabel triesNumber(){
        tries = new JLabel("Number of tries : 0");
        return tries;
    }
    
    //method to define what happen if a button is pressed
    public void click(){
        //if jbutton not clicked, reveal the card. else, close the card
        if(this.clicked == false){
            this.card.setIcon(pic);
            clicked = true;
            opened.add(this);
        }
        else{
            this.card.setIcon(picDef);
            clicked = false;
            for(int i = 0; i < opened.size(); i++){
                if(opened.get(i).getClicked() == false){
                    opened.remove(i);
                }
            }
        }
    }
    
    //method to compare two jbutton
    public void check(){
        //if user click a button when program is processing
        if(opened.size() != 2){
            throw new EmptyStackException();
        }
        //if button are identical, disable the button. else, close both card
        if(opened.get(0).getNum().equals(opened.get(1).getNum())){
            opened.get(0).getCard().setEnabled(false);
            opened.get(1).getCard().setEnabled(false);
            win += 1;
        }
        else{
            opened.get(0).getCard().setIcon(picDef);
            opened.get(0).setClicked(false);
            opened.get(1).getCard().setIcon(picDef);
            opened.get(1).setClicked(false);
        }
        
        opened.clear();
        
        checkWin();
    }
    
    //method to check win condition
    public void checkWin(){
        //if 18 pairs are revealed, user won the game
        if(win == 18){
            JOptionPane.showMessageDialog(null, "You have won this game in " 
            + triesNum + " tries!", "CONGRATULATIONS!", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    //method to reset jbutton picture and id
    public void reset(String picNum){
        this.picNum = picNum;
        this.getCard().setEnabled(true);
        this.clicked = true;
        this.click();
        try {
            BufferedImage pict = ImageIO.read(new File("image\\cover.png"));
            this.picDef = new ImageIcon(pict);
            pict = ImageIO.read(new File("image\\" + picNum + ".jpg"));
            this.pic = new ImageIcon(pict);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    //method to reset all moves done by user
    public static void resetAll(){
        win = 0;
        triesNum = 0;
        tries.setText("Number of tries : 0");
    }
}